import model.Fibonacci;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Fibonacci fibonacci = new Fibonacci();
        int n;

        Scanner in = new Scanner(System.in);
        System.out.println("Введите количество элементов массива: ");
        n = in.nextInt();

        fibonacci.setN(n);

        fibonacci.fillArray();

        fibonacci.printFib();
    }
}