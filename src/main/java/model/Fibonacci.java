package model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;

public class Fibonacci
{
    private BigInteger[] fibArr;
    private int n;

    public BigInteger[] getFibArr()
    {
        return fibArr;
    }

    public int getN()
    {
        return n;
    }

    public Fibonacci()
    {

    }

    public void setFibArr(BigInteger[] fibArr)
    {
        this.fibArr = fibArr;
    }

    public void setN(int n)
    {
        this.n = n;
        initArr();
    }

    private void initArr()
    {
        fibArr = new BigInteger[n];
    }

    public void fillArray()
    {
        Logger logger = LoggerFactory.getLogger(Fibonacci.class);
        logger.info("Начало инициализации массива Фибоначчи.");
        long startTime = System.currentTimeMillis();
        fibArr[0] = BigInteger.ZERO;
        fibArr[1] = BigInteger.ONE;
        int i = 2;
        while (i<fibArr.length)
        {
            fibArr[i] = fibArr[i-1].add(fibArr[i-2]);
            i++;
        }

        logger.info("Массив проинициализирован за " + (System.currentTimeMillis() - startTime) + " миллисекунд.");

    }

    public void printFib()
    {
        for (int i = 0; i < fibArr.length; i++)
        {
            System.out.println("["+i+"] элемент последовательности Фибоначчи: " + fibArr[i]);
        }
    }
}
